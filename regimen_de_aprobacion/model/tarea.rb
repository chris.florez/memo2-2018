class Tarea

  attr_accessor :nombre
  def initialize
    @alumnos_aprobados = Array.new
  end
  attr_reader :alumnos_aprobados

  def aprobar(alumno)
    @alumnos_aprobados.push(alumno)
  end

  def aprobado?(alumno)
    @alumnos_aprobados.each do |item|
      if item.nombre == alumno.nombre
        return true
      end
    end
  end

end