class Curso

  NUM_ITERACIONES_APROBADAS = 3
  PORCENTAJE_ASISTENCIA_MIN = 75

	def initialize
		@tareas = Array.new
		@alumnos = Array.new
		@proyecto_pg_aprobado = Array.new
	end

	attr_accessor :nombre
	attr_reader :tareas
	attr_reader :alumnos
	attr_reader :proyecto_pg_aprobado

  def inscribir(alumno)
    @alumnos.push(alumno)
	end

	def agregar_tarea(tarea)
		@tareas.push(tarea)
	end

  def aprobo_proyecto?(num_iteraciones, alumno)
    @proyecto_pg_aprobado.each do |item|
      if item.nombre == alumno.nombre
        if num_iteraciones <= NUM_ITERACIONES_APROBADAS
          @proyecto_pg_aprobado.delete(alumno)
          return false
        end
        return true
      end
    end
    if num_iteraciones >= NUM_ITERACIONES_APROBADAS
      @proyecto_pg_aprobado.push(alumno)
      return true
    end
    false
  end

	def aprobar_tarea?(tarea, alumno)
		@tareas.each do |item|
      if item.nombre == tarea.nombre
        item.aprobar(alumno)
        return true
      end
    end
    false
	end

  def cumple_asistencia?(alumno)
    @alumnos.each do |item|
      if item.nombre == alumno.nombre && item.porcentaje_asistencia >= PORCENTAJE_ASISTENCIA_MIN
        return true
      end
    end
    false
  end

  def aprobo_todas_las_tareas?(alumno)
    @tareas.each do |item|
      unless item.aprobado?(alumno)
        return false
      end
    end
    true
  end

  def aprobo_iteraciones_proyecto?(alumno)
    @proyecto_pg_aprobado.each do |item|
      if item.nombre == alumno.nombre
          return true
      end
    end
    false
  end

  def alumno_aprobado?(alumno)
     cumple_asistencia?(alumno) && aprobo_todas_las_tareas?(alumno) && aprobo_iteraciones_proyecto?(alumno)
  end
end

