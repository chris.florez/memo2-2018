require 'rspec'
require_relative '../model/alumno'

describe 'Alumno' do

  let(:un_alumno) { Alumno.new }

  it 'se crea un alumno con nombre Juan, por lo tanto retorna Juan' do
    un_alumno.nombre = 'Juan'
    expect(un_alumno.nombre).to eq 'Juan'
  end

  it 'el alumno Juan tiene porcentaje de asistencia de 75' do
    un_alumno.porcentaje_asistencia = 75
    expect(un_alumno.porcentaje_asistencia).to eq 75
  end

end