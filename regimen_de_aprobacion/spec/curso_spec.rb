require 'rspec'
require_relative '../model/curso'

describe 'Curso' do

  let(:curso_1) { Curso.new }
  let(:un_alumno) { Alumno.new }
  let(:ta) { Tarea.new }
  let(:tb) { Tarea.new }
  let(:tc) { Tarea.new }

  it 'se crea un curso de algebra' do
    curso_1.nombre = 'algebra'
    expect(curso_1.nombre).to eq 'algebra'
  end

  it 'el curso no tiene alumnos, por lo tanto retorna ' do
    expect(curso_1.alumnos.empty?).to eq true
  end

  it 'en el curso se inscribe a el alumno Juan, por lo tanto no esta vacio' do
    un_alumno.nombre = 'Juan'
    curso_1.inscribir(un_alumno)
    expect(curso_1.alumnos.empty?).to eq false
  end

  it 'el alumno Juan aprobo 3 iteraciones del proyecto, por lo tanto aprobo el proyecto del curso' do
    un_alumno.nombre = 'Juan'
    curso_1.inscribir(un_alumno)
    expect(curso_1.aprobo_proyecto?(3, un_alumno)).to eq true
  end

  it 'el alumno Juan aprobo 2 iteraciones del proyecto, por lo tanto aprobo el proyecto del curso' do
    un_alumno.nombre = 'Juan'
    curso_1.inscribir(un_alumno)
    expect(curso_1.aprobo_proyecto?(2, un_alumno)).to eq false
  end

  it 'se agrega la tarea ta al curso de algebra' do
    ta.nombre = 'ta'
    un_alumno.nombre = 'Juan'
    curso_1.inscribir(un_alumno)
    curso_1.agregar_tarea(ta)
    expect(curso_1.tareas).not_to eq nil
  end

  it 'el alumno Juan aprobo la tarea ta, del curso de algebra' do
    ta.nombre = 'ta'
    un_alumno.nombre = 'Juan'
    curso_1.inscribir(un_alumno)
    curso_1.agregar_tarea(ta)
    expect(curso_1.aprobar_tarea?(ta, un_alumno)).to eq true
  end

  it 'hay un alumno en el curso, y su porcentaje de asistencia es de 60' do
    un_alumno.porcentaje_asistencia = 60
    curso_1.inscribir(un_alumno)
    expect(curso_1.cumple_asistencia?(un_alumno)).to eq false
  end

  it 'hay un alumno en el curso, y su porcentaje de asistencia es de 75' do
    un_alumno.porcentaje_asistencia = 75
    curso_1.inscribir(un_alumno)
    expect(curso_1.cumple_asistencia?(un_alumno)).to eq true
  end

  it 'el alumno Juan aprobo 3 iteraciones del proyecto, por lo tanto aprobo el proyecto del curso' do
    un_alumno.nombre = 'Juan'
    curso_1.inscribir(un_alumno)
    curso_1.aprobo_proyecto?(3,un_alumno)
    expect(curso_1.aprobo_iteraciones_proyecto?(un_alumno)).to eq true
  end

  it 'el alumno Juan aprobo todas las tareas' do
    ta.nombre = 'ta'
    tb.nombre = 'tb'
    tc.nombre = 'tc'
    curso_1.agregar_tarea(ta)
    curso_1.agregar_tarea(tb)
    curso_1.agregar_tarea(tc)
    un_alumno.nombre = 'Juan'
    curso_1.inscribir(un_alumno)
    curso_1.aprobar_tarea?(ta,un_alumno)
    curso_1.aprobar_tarea?(tb,un_alumno)
    curso_1.aprobar_tarea?(tc,un_alumno)
    expect(curso_1.aprobo_todas_las_tareas?(un_alumno)).to eq true
  end

  it 'el alumno Juan aprobo todas las tareas, 3 iteraciones del proyecto pg y tiene 80% de asistencia' do
    ta.nombre = 'ta'
    tb.nombre = 'tb'
    tc.nombre = 'tc'
    curso_1.agregar_tarea(ta)
    curso_1.agregar_tarea(tb)
    curso_1.agregar_tarea(tc)
    un_alumno.nombre = 'Juan'
    curso_1.inscribir(un_alumno)
    curso_1.aprobar_tarea?(ta,un_alumno)
    curso_1.aprobar_tarea?(tb,un_alumno)
    curso_1.aprobar_tarea?(tc,un_alumno)
    un_alumno.porcentaje_asistencia = 80
    curso_1.aprobo_proyecto?(3,un_alumno)
    expect(curso_1.alumno_aprobado?(un_alumno)).to eq true
  end

end