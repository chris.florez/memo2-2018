require 'rspec' 
require_relative '../model/tarea'

describe 'Tarea' do

  let(:tarea_A) { Tarea.new }
  it 'se crea una tarea que se llama ta ' do
    tarea_A.nombre = 'ta'
    expect(tarea_A.nombre).to eq 'ta'
  end

  let(:alumno) {Alumno.new}
  it 'el alumno Juan aprueba la tarea ta, por lo tanto la lista de aprobados de ta no esta vacia' do
    alumno.nombre = 'Juan'
    tarea_A.aprobar(alumno)
    expect(tarea_A.aprobado?(alumno)).to eq true
  end
end