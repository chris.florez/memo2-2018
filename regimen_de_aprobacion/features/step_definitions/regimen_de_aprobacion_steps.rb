require_relative '../../model/tarea'
require_relative '../../model/curso'
require_relative '../../model/alumno'

$algebra = Curso.new
$algebra.nombre = 'algebra'
$ta = Tarea.new
$ta.nombre = 'ta'
$tb = Tarea.new
$tb.nombre = 'tb'
$tc = Tarea.new
$tc.nombre = 'tc'
$algebra.agregar_tarea($ta)
$algebra.agregar_tarea($tb)
$algebra.agregar_tarea($tc)
$un_alumno = Alumno.new

Given(/^el alumno regular Juan$/) do
  $un_alumno.nombre = 'Juan'
  $algebra.inscribir($un_alumno)
end

When(/^aprobo todas las tareas semanales$/) do
  $algebra.aprobar_tarea?($ta, $un_alumno)
  $algebra.aprobar_tarea?($tb, $un_alumno)
  $algebra.aprobar_tarea?($tc, $un_alumno)
end

When(/^asistio al (\d+) % de las clases$/) do |porcentaje_asistencia|
  $un_alumno.porcentaje_asistencia = porcentaje_asistencia.to_i
end

When(/^aprobo al menos (\d+) iterationes del proyecto pg$/) do |cant_iteraciones|
  $algebra.aprobo_proyecto?(cant_iteraciones.to_i, $un_alumno)
end

Then(/^aprobo la materia$/) do
  expect($algebra.alumno_aprobado?($un_alumno)).to eq true
end


When(/^aprobo al menos (\d+) iteration del proyecto pg$/) do |cant_iteraciones|
  $algebra.aprobo_proyecto?(cant_iteraciones.to_i, $un_alumno)
end

When(/^no aprobo la tarea individual tc$/) do
  $algebra.aprobar_tarea?($ta, $un_alumno)
  $algebra.aprobar_tarea?($tb, $un_alumno)
end

Then(/^No aprobo la materia$/) do
  expect($algebra.alumno_aprobado?($un_alumno)).to eq false
end

When(/^aprobo (\d+) iterationes del proyecto pg$/) do |cant_iteraciones|
  $algebra.aprobo_proyecto?(cant_iteraciones.to_i, $un_alumno)
end