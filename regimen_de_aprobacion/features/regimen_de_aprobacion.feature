Feature: Regimen de aprobacion

 Scenario: alumno aprobado
 Given el alumno regular Juan
 When aprobo todas las tareas semanales
 And asistio al 75 % de las clases
 And aprobo al menos 3 iterationes del proyecto pg
 Then aprobo la materia

 Scenario: alumno desaprobado por inasistencias
 Given el alumno regular Juan
 When aprobo todas las tareas semanales
 And asistio al 60 % de las clases
 And aprobo al menos 3 iteration del proyecto pg
 Then No aprobo la materia

 Scenario: alumno desaprobado por tareas individuales
 Given el alumno regular Juan
 When no aprobo la tarea individual tc
 Then No aprobo la materia

 Scenario: alumno desaprobado por proyecto grupal
 Given el alumno regular Juan
 When aprobo todas las tareas semanales
 And asistio al 75 % de las clases
 And aprobo 2 iterationes del proyecto pg
 Then No aprobo la materia