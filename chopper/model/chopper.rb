class Chopper

  def chop(n, array)
  	cont = 0
  	array.each do |elemento|
  		if elemento == n
  			return cont
  		end
  		cont += 1
  	end
    return -1
  end

  def sum(array)
  	
	numeros = [ 'cero', 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siente', 'ocho', 'nueve' ]  
  	
  	sumatoria = 0
  	
  	if array.length == 0
		return 'vacio'
	end
		
  	array.each do |elemento|
  		sumatoria += elemento
  	end
    
    divisor = sumatoria/10 
  	resto = sumatoria%10
  	
  	if divisor > 0 
  		if sumatoria == 100
  			return 'demasiado grande'
  		end	
  		return numeros[divisor] + ',' + numeros[resto]
		
  	else 
  		return numeros[resto]
  	end

  end
  		
end
