require 'rspec' 
require_relative '../model/isbn'

describe 'Isbn' do

  let(:num_isbn) { Isbn.new }  
   
  it 'isbn string 04719586972 tiene longitud mayor a 10' do
    expect(num_isbn.verificar_codigo_isbn?('04719586972')).to eq false
  end

  it 'isbn string 047195869721 tiene longitud menor a 10' do
    expect(num_isbn.verificar_codigo_isbn?('04719586')).to eq false
  end

  it 'isbn string 0471958697 tiene longitud igual a 10, y la longitud valida es de 10' do
    expect(num_isbn.verificar_codigo_isbn?('0471958697')).to eq true
  end 

  it 'el formato isbn 0.471.95869.7 no es valido' do
    expect(num_isbn.verificar_codigo_isbn?('0.471.95869.7')).to eq false
  end

  it 'el formato isbn 0.471.95.7 no es valido, pero tiene longitud 10' do
    expect(num_isbn.verificar_codigo_isbn?('0.471.95.7')).to eq false
  end

  it 'el formato isbn 0-471-95869-7 es valido, filtrado tiene longitud 10' do
    expect(num_isbn.verificar_codigo_isbn?('0-471-95869-7')).to eq true
  end

  it 'isbn string 0 471 95869 7 es valido, filtrado tiene longitud 10' do
    expect(num_isbn.verificar_codigo_isbn?('0 471 95869 7')).to eq true
  end

  it 'isbn string 0471958698 el numero de control no es 8' do
    expect(num_isbn.verificar_codigo_isbn?('0471958698')).to eq false
  end

  it 'isbn string 0 471 95869 7 verificar que es un codigo isbn' do
    expect(num_isbn.verificar_codigo_isbn?('0 471 95869 7')).to eq true
  end

  it 'isbn string 155404295X verificar que es un codigo isbn' do
    expect(num_isbn.verificar_codigo_isbn?('155404295X')).to eq true
  end

end
