class Isbn
  LONGITUD = 10
  FILTRO = /^[\s0-9Xx-]*$/
  ERROR = "isbn_invalido"  
  
  def verificar_codigo_isbn?(cadena_isbn)
    #la cadena filtrada deberia contener 10 caractares
    cadena_filtrada = filtrar_caracteres_separadores(cadena_isbn)
    if verificar_longitud?(cadena_filtrada)
      resultado_operacion = calcular_operacion_previa(cadena_filtrada)
      resto = resultado_operacion.modulo(11)
      if resto < LONGITUD  
        return resto == cadena_filtrada[cadena_filtrada.length-1].to_i 
      end 
      return 'X' == cadena_filtrada[cadena_filtrada.length-1] 
    end 
    return false
  end 

  private
  def verificar_longitud?(cadena_isbn)
    cadena_filtrada = filtrar_caracteres_separadores(cadena_isbn)    
    if cadena_filtrada != ERROR
      return cadena_filtrada.length == LONGITUD
    end   
  end

  private
  def filtrar_caracteres_separadores(cadena_isbn)
    resultado = cadena_isbn.match(FILTRO)
    if (resultado != nil)
        return cadena_isbn.delete "-  "     
    end   
    return ERROR
  end 

  private
  def calcular_operacion_previa(cadena_isbn)
      pos = 0
      resultado = 0
      cadena_filtrada = filtrar_caracteres_separadores(cadena_isbn)
      while pos < cadena_filtrada.length-1
        resultado += cadena_filtrada[pos].to_i*(pos+1)
        pos += 1
      end
      resultado
  end
end