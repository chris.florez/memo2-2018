require 'rspec'
require_relative '../model/calculadora'

describe 'Calculadora' do

  let(:calculadora) {Calculadora.new}

  it 'se pasa cantidad y precio, devuelve el monto total' do
    expect(calculadora.calcular_descuento(9, 100)).to eq 900
  end

  it 'se pasa una cantidad y un precio con decimales, devuelve el monto total' do
    expect(calculadora.calcular_descuento(10, 99.49)).to eq 994.9
  end

  it 'se pasa una cantidad y precio, de tal manera que tenga descuento el monto total' do
    expect(calculadora.calcular_descuento(11, 100)).to eq 1067
  end

  it 'se pasa una cantidad y precio con decimales, devuelve el monto total a pagar con los descuentos' do
    expect(calculadora.calcular_descuento(10, 100.49)).to eq 974.75
  end

  it 'se pasa una cantidad y precio que es un total de $5000, devuelve el monto total con un descuento del 5%' do
    expect(calculadora.calcular_descuento(50, 100)).to eq 4750
  end

  it 'Se pasa una cantidad y precio que supera los $7000, devuelve el monto total con un descuento del 7%' do
    expect(calculadora.calcular_descuento(80, 100)).to eq 7440
  end

  it 'Se pasa una cantidad y precio que llega a $10000, devuelve el monto total con un descuento del 10%' do
    expect(calculadora.calcular_descuento(10, 1000)).to eq 9000
  end

  it 'Se pasa una cantidad y precio que llega a $50000, devuelve el monto total con un descuento del 15%' do
    expect(calculadora.calcular_descuento(50, 1000)).to eq 42500
  end

  it 'si se pasa una cantidad y/o precio negativos, devuelve operacion invalida' do
    expect(calculadora.calcular_monto(-10, 100, 'UT')).to eq "operacion_invalida"
  end

  it 'se pasa una cantidad, precio y un codigo de estado, dicho monto final tiene solo el impuesto del 6.85%' do
    expect(calculadora.calcular_monto(9, 100, 'UT')).to eq 961.65
  end

  it 'se pasa una cantidad, precio y un codigo de estado, dicho monto final tiene el descuento del 3% y el impuesto del 8%' do
    expect(calculadora.calcular_monto(10, 100, 'NV')).to eq 1047.6
  end

  it 'se pasa una cantidad, precio y un codigo de estado, el monto final tiene un descuento del 3% y el impuesto del 6.25%' do
    expect(calculadora.calcular_monto(10, 100, 'TX')).to eq 1030.63
  end

  it 'se pasa una cantidad, precio y un codigo de estado, dicho monto final tiene el descuento del 3% y el impuesto del 4%' do
    expect(calculadora.calcular_monto(10, 100, 'AL')).to eq 1008.8
  end

  it 'se pasa una cantidad, precio y un codigo de estado, dicho monto final tiene el descuento del 3% y el impuesto del 8.25%' do
    expect(calculadora.calcular_monto(50, 1000, 'CA')).to eq 46006.25
  end

end
