# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

describe "Sinatra Application" do

  it "se hace un POST /calcular_descuento -d x=10&y=100, deberia retornar 900.0" do
    post '/calcular_descuento', {'x': '9', 'y': '100'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=9,y=100","resultado":"900.0"}'
  end

  it "se hace un POST /calcular_descuento -d x=10&y=99.49, deberia retornar 994.9" do
    post '/calcular_descuento', {'x': '10', 'y': '99.49'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=10,y=99.49","resultado":"994.9"}'
  end

  it "se hace un POST /calcular_descuento -d x=11&y=100, deberia retornar 1067.0" do
    post '/calcular_descuento', {'x': '11', 'y': '100'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=11,y=100","resultado":"1067.0"}'
  end

  it "se hace un POST /calcular_descuento -d x=10&y=100.49, deberia retornar 974.75" do
    post '/calcular_descuento', {'x': '10', 'y': '100.49'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=10,y=100.49","resultado":"974.75"}'
  end

  it "se hace un POST /calcular_descuento -d x=50&y=100, deberia retornar 4750.0" do
    post '/calcular_descuento', {'x': '50', 'y': '100'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=50,y=100","resultado":"4750.0"}'
  end

  it "se hace un POST /calcular_descuento -d x=80&y=100, deberia retornar 7440.0" do
    post '/calcular_descuento', {'x': '80', 'y': '100'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=80,y=100","resultado":"7440.0"}'
  end

  it "se hace un POST /calcular_descuento -d x=10&y=1000, deberia retornar 9000" do
    post '/calcular_descuento', {'x': '10', 'y': '1000'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=10,y=1000","resultado":"9000.0"}'
  end

  it "se hace un POST /calcular_descuento -d x=50&y=1000, deberia retornar 42500" do
    post '/calcular_descuento', {'x': '50', 'y': '1000'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=50,y=1000","resultado":"42500.0"}'
  end

  it "se hace un POST /calcular_monto_final -d x=-10&y=1000&cod=UT, deberia retornar operacion_invalida" do
    post '/calcular_monto_final', {'x': '-10', 'y': '1000', 'cod': 'UT'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=-10,y=1000,cod=UT","resultado":"operacion_invalida"}'
  end

  it "se hace un POST /calcular_monto_final -d x=9&y=100&cod=UT, deberia retornar operacion_invalida" do
    post '/calcular_monto_final', {'x': '9', 'y': '100', 'cod': 'UT'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=9,y=100,cod=UT","resultado":"961.65"}'
  end

  it "se hace un POST /calcular_monto_final -d x=50&y=1000&cod=TX, deberia retornar operacion_invalida" do
    post '/calcular_monto_final', {'x': '50', 'y': '1000', 'cod': 'TX'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"calcular_descuento":"x=50,y=1000,cod=TX","resultado":"45156.25"}'
  end


end