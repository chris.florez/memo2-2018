require 'sinatra'
require 'json'
require './model/calculadora'

post '/calcular_descuento' do
  cantidad = params["x"]
  precio = params["y"]
  calculadora = Calculadora.new
  resultado = calculadora.calcular_descuento(cantidad.to_f, precio.to_f)
  content_type :json
  {:calcular_descuento => "x=#{cantidad},y=#{precio}", :resultado => "#{resultado}"}.to_json
end

post '/calcular_monto_final' do
  cantidad = params["x"]
  precio = params["y"]
  cod_estado = params["cod"]
  calculadora = Calculadora.new
  resultado = calculadora.calcular_monto(cantidad.to_f, precio.to_f, cod_estado)
  content_type :json
  {:calcular_descuento => "x=#{cantidad},y=#{precio},cod=#{cod_estado}", :resultado => "#{resultado}"}.to_json
end