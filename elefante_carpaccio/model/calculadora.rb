class Calculadora

  ERROR_OPERACION = 'operacion_invalida'
  ERROR_ESTADO = 'estado no valido'
  TOTAL = 1
  DESCUENTO_0 = 0
  MONTO_1 = 1000
  DESCUENTO_1 = 0.03
  MONTO_2 = 5000
  DESCUENTO_2 = 0.05
  MONTO_3 = 7000
  DESCUENTO_3 = 0.07
  MONTO_4 = 10000
  DESCUENTO_4 = 0.1
  MONTO_5 = 50000
  DESCUENTO_5 = 0.15
  ESTADO_1 = "UT"
  ESTADO_2 = "NV"
  ESTADO_3 = "TX"
  ESTADO_4 = "AL"
  ESTADO_5 = "CA"
  IMPUESTO_1 = 1.0685
  IMPUESTO_2 = 1.08
  IMPUESTO_3 = 1.0625
  IMPUESTO_4 = 1.04
  IMPUESTO_5 = 1.0825


  def initialize
    @montos = [MONTO_1, MONTO_2, MONTO_3, MONTO_4, MONTO_5]
    @descuentos = [DESCUENTO_0, DESCUENTO_1, DESCUENTO_2, DESCUENTO_3, DESCUENTO_4, DESCUENTO_5]
    @impuestos = {ESTADO_1 => IMPUESTO_1, ESTADO_2 => IMPUESTO_2, ESTADO_3 => IMPUESTO_3, ESTADO_4 => IMPUESTO_4, ESTADO_5 => IMPUESTO_5}
  end

  def calcular_monto(cantidad, precio, cod_estado)
    if cantidad < 0 || precio < 0
      ERROR_OPERACION
    else
      impuesto = @impuestos[cod_estado]
      if impuesto.nil?
        ERROR_ESTADO
      else
        (calcular_descuento(cantidad, precio) * impuesto).round(2)
      end
    end
  end

  def calcular_descuento(cantidad, precio)
    monto_parcial = cantidad * precio
    pos = 0
    while pos < @montos.length
      if monto_parcial < @montos[pos]
        return (monto_parcial * (TOTAL - @descuentos[pos])).round(2)
      end
      pos += 1
    end
    if pos == @montos.length
      (monto_parcial * (TOTAL - @descuentos[pos])).round(2)
    end
  end

end
