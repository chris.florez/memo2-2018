require 'sinatra'
require 'json'
require './model/chopper'

get '/sum' do
  entrada = params["x"]
  chopper = Chopper.new
  resultado = chopper.sum(entrada.split(",").map(&:to_i))
  content_type :json
  { :sum => "#{entrada}", :resultado => "#{resultado}"}.to_json
end

post '/chop' do
  numero = params["x"]
  vector = params["y"]
  chopper = Chopper.new
  resultado = chopper.chop(numero.to_i,vector.split(",").map(&:to_i))
  content_type :json
  { :chop => "x=#{numero},y=#{vector}", :resultado => "#{resultado}"}.to_json
  
end
