# spec/app_spec.rb

require File.expand_path '../spec_helper.rb', __FILE__

describe "Sinatra Application" do

  it "se hace un GET /sum?x=9,9, deberia retornar uno,ocho" do
    get '/sum?x=9,9'
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"sum":"9,9","resultado":"uno,ocho"}'
  end

  it "GET /sum?x=7,9" do
    get '/sum?x=7,9'
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"sum":"7,9","resultado":"uno,seis"}'
  end

  it "se hace un POST /chop -d x=3&y=0,7,3, deberia retornar 2" do
    post '/chop',  {'x':'3','y':'0,7,3'}
    expect(last_response).to be_ok
    expect(last_response.body).to include '{"chop":"x=3,y=0,7,3","resultado":"2"}'
  end 

end